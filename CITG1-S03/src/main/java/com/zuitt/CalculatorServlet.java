package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	public void init() throws ServletException{
		System.out.println("**********************");
		System.out.println("Initialized connection to database.");
		System.out.println("**********************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		System.out.println("Hello from the calculator servlet.");
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String operation = req.getParameter("operation");
		
		int result;
		switch (operation) {
			case "add":
				result = num1 + num2;
				PrintWriter out = res.getWriter();
				out.println("The numbers you provided are: " + num1 + ", " + num2 + "\n");
				out.println("The operation that you wanted is: " + operation + "\n");
				out.println("The result is: " + result + "\n");
				break;
				
			case "subtract":
				result = num1 - num2;
				PrintWriter out1 = res.getWriter();
				out1.println("The numbers you provided are: " + num1 + ", " + num2 + "\n");
				out1.println("The operation that you wanted is: " + operation + "\n");
				out1.println("The result is: " + result + "\n");
				break;

			case "multiply":
				result = num1 * num2;
				PrintWriter out2 = res.getWriter();
				out2.println("The numbers you provided are: " + num1 + ", " + num2 + "\n");
				out2.println("The operation that you wanted is: " + operation + "\n");
				out2.println("The result is: " + result + "\n");
				break;

			case "divide":
				result = num1 / num2;
				PrintWriter out3 = res.getWriter();
				out3.println("The numbers you provided are: " + num1 + ", " + num2 + "\n");
				out3.println("The operation that you wanted is: " + operation + "\n");
				out3.println("The result is: " + result + "\n");
				break;
				
			default:
				PrintWriter out4 = res.getWriter();
				out4.println("Wrong operation");
		}
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app.</h1>");
		out.println("To use the app, input two numbers and an operation.<br><br>");
		out.println("Hit the submit button after filling in the details.<br><br>");
		out.println("You will get the result shown in your browser!\n");
	}
	
	public void destroy() {
		System.out.println("**********************");
		System.out.println("Disconnected from database.");
		System.out.println("**********************");
	}
}
