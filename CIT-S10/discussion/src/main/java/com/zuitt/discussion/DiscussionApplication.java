package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	// Get all users
//	localhost:8080/users
//	@RequestMapping(value="/users", method = RequestMethod.GET)
	@GetMapping("/users")
	public String getUsers(){
		return "All users retrieved";
	}

	// Create User
//	localhost:8080/users
//	@RequestMapping(value = "/users", method = RequestMethod.POST)
	@PostMapping("/users")
	public String createUser(){
		return "New user created.";
	}

	// Get a specific user
//	localhost:8080/myUsers
//	@RequestMapping(value = "/myUsers", method = RequestMethod.GET)
	@GetMapping("/myUsers")
	public String getMyUsers(@RequestHeader(value = "Authorization") String user){
		return "Users for " + user + " have been retrieved";
	}

	// Delete a user
//	localhost:8080/users/1234
//	@RequestMapping(value = "users/{userid}", method = RequestMethod.DELETE)
	@DeleteMapping("/users/{userid}")
	public String deleteUser(@RequestHeader(value = "Authorization") String userid){
		if (userid.equals("john")) {
			return "The user 1234 has been deleted.";
		} else {
			return "Unauthorized access";
		}
	}

	// Update user
//	localhost:8080/users/1234
//	@RequestMapping(value = "/posts/{userid}", method = RequestMethod.PUT)
//	Automatically converts the format to JSON.
	@PutMapping("/users/{userid}")
	@ResponseBody
	public Users updateUser(@PathVariable Long userid, @RequestBody Users user){
		return user;
	}
}
