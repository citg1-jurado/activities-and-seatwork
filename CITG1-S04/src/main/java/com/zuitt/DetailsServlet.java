package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;
	
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
//		a. Branding - Servlet Context parameter
		ServletContext srvContext = getServletContext();		   //Servlet Context Parameter
		String branding = srvContext.getInitParameter("branding"); //Servlet Context Parameter
		
//		b. First name - System Properties
		String firstname = System.getProperty("firstname"); //System Properties
		
//		c. Last name - HttpSession
		HttpSession session = req.getSession();						   //HttpSession
		String lastname = session.getAttribute("lastname").toString(); //HttpSession
		
//		d. Email - Servlet Context
		String email = srvContext.getAttribute("email").toString(); //Servlet Context
		
//		e. Contact Number - Url Rewriting via SendRedirect
		String contact = req.getParameter("contact"); //SendRedirect
		
		PrintWriter output = res.getWriter();
		output.println(
				"<h1>" + branding + "</h1>" +
				"<p>First Name: " + firstname + "</p>" +
				"<p>Last Name: " + lastname + "</p>" +
				"<p>Contact: " + contact + "</p>" +
				"<p>Email: " + email + "</p>"
				);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		String contact = req.getParameter("contact");
		res.sendRedirect("details?contact=" + contact);
	}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been destroy. ");
		System.out.println("******************************************");
	}
	
}
