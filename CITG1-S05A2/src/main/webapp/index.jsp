<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Job Finder App</title>
</head>
<body>
	<div id="container">
	
		<h1>Welcome to Servlet Job Finder!</h1>
		
			<form action="register" method="post">
				
				<!-- First Name -->
				<div>
					<label for="firstname">First Name</label>
					<input type="text" name="firstname" required>
				</div>
				
				<!-- Last Name -->
				<div>
					<label for="lastname">Last Name</label>
					<input type="text" name="lastname" required>
				</div>
				
				<!-- Phone -->
				<div>
					<label for="phone">Phone</label>
					<input type="tel" name="phone" required>
				</div>
				
				<!-- Email -->
				<div>
					<label for="email">Email</label>
					<input type="email" name="email" required>
				</div>
				
				<fieldset>
					<legend>How did you discover the app?</legend>
					
					<!-- Friends -->
					<input type="radio" id="friends" name="app_type" value="friends" required>
					<label for="friends">Friends</label>
					
					<!-- Social Media -->
					<input type="radio" id="social_media" name="app_type" value="social_media" required>
					<label for="social_media">Social Media</label>
					
					<!-- Others -->
					<input type="radio" id="others" name="app_type" value="others" required>
					<label for="others">Social Media</label>
				</fieldset>
				
				<!-- Date of Birth -->
				<div>
					<label for="date_of_birth">Date of birth</label>
					<input type="date" name="date_of_birth" required>
				</div>
				
				<!-- Applicant or Employee -->
				<div>
					<label for="emap">Are you an employer or applicant?</label>
					<input type="text" name="emap" required list="emaps">
					<!-- datalist provide autocomplete feature -->
					<datalist id="emaps">
						<option value="Employer">
						<option value="Applicant">
					</datalist>
				</div>
				
				<!-- Profile Description -->
				<div>
					<label for="comments">Profile Description</label>
					<textarea name="comments" maxlength="500"></textarea>
				</div>
				
				<!-- Button for Submit -->
				<button>Register</button>
			</form>
	
	</div>
</body>
</html>