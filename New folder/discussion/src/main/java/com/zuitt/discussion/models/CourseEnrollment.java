package com.zuitt.discussion.models;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="courseEnrollments")
public class CourseEnrollment {

    @Id
    @GeneratedValue
    private int id;

    @Column
    private int courseId;

    @Column
    private int userId;

    @Column
    private LocalDateTime dateTimeEnrolled;

    public CourseEnrollment(){};

    public CourseEnrollment(int courseId, int userId, LocalDateTime dateTimeEnrolled) {
        this.courseId = courseId;
        this.userId = userId;
        this.dateTimeEnrolled = dateTimeEnrolled;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public LocalDateTime getDateTimeEnrolled() {
        return dateTimeEnrolled;
    }

    public void setDateTimeEnrolled(LocalDateTime dateTimeEnrolled) {
        this.dateTimeEnrolled = dateTimeEnrolled;
    }
}
