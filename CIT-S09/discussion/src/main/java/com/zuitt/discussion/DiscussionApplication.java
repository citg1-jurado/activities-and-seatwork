package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
//This application will function as an endpoint that will be used in handling http request.
@RestController
//Will require all routes within the class to use the set endpoint as part of its route.
@RequestMapping("/greeting")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

//	ctrl + c to stop the server and application

//	localhost:8080/hello
	@GetMapping("/hello")
//	Maps a get request to the route "/hello" and invokes the method hello().
	public String hello(){
		return "Hello World";
	}

//	Route with String Query
//	localhost:8080/hi?name=value
	@GetMapping("/hi")
//	"@RequestParam" annotation that allows us to extract data from query strings in the URL
	public String hi(@RequestParam(value="name", defaultValue = "John") String name){
		return String.format("Hi %s", name);
	}

//	Multiple parameters
//	localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name", defaultValue = "Joe") String name, @RequestParam(value="friend", defaultValue = "Jane") String friend){
		return  String.format("Hello %s! My name is %s.", friend, name);
	}

//	Route with path variables
//	Dynamic data is obtained directly from the url
//	localhost:8080/name
	@GetMapping("/hello/{name}")
//	"@PathVariable" annotation allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you %s!", name);
	}

//	Activity for s09
//	1. Initialize a new ArrayList called enrollees in the DiscussionApplication class.
	ArrayList<String> enrollees = new ArrayList<>();

//	2. Create a /enroll route that will accept a query string with the parameter of user.
//	http://localhost:8080/greeting/enroll?user=value
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value="user") String user){
		enrollees.add(user);
		return "Thank you for enrolling, " + user + "!";
	}

//	3. Create a new /getEnrollees route which will return the content of the enrollees ArrayList as a string.
//	http://localhost:8080/greeting/getEnrollees
	@GetMapping("/getEnrollees")
	public String getEnrollees() {
		return enrollees.toString();
	}

//	4. Create a /nameage route with that will accept multiple query string parameters of name and age and will return a Hello (name)! My age is (age). message.
//	http://localhost:8080/greeting/nameage?name=value&age=value
	@GetMapping("/nameage")
	public String getNameAge(
			@RequestParam("name") String name,
			@RequestParam("age") int age
	) {
		return "Hello " + name + "! My age is " + age + ".";
	}

//	5. Create a /course/id dynamic route using a path variable of id.
//	http://localhost:8080/greeting/courses/id
	@GetMapping("/courses/{id}")
	public String getCourseDetails(@PathVariable String id) {
		if (id.equals("java101")) {
			return "Java 101, MWF 8:00AM-11:00AM, PHP 3000.00";
		} else if (id.equals("sql101")) {
			return "SQL 101, TTH 1:00PM-4:00PM, PHP 2000.00";
		} else if (id.equals("javaee101")) {
			return "Java EE, MWF 1:00PM-4:00PM, PHP 3500.00";
		} else {
			return "Course " + id + " cannot be found.";
		}
	}
}
