package com.zuitt.discussion.models;

import javax.persistence.*;

// Marks this java object as a representation of an entity/record from the database table "posts".
@Entity
//Designate the table name related to the model.
@Table(name = "courses")
public class Course {

    //    Properties
//    Indicates the primary key
    @Id
//    id will be auto-incremented.
    @GeneratedValue
    private Long id;

    //    Class properties that represents table column in a relational database are annotated as @Column
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private Double price;

    //    establishes the relationship of the property to the "course" model
    @ManyToOne
//    sets the relationship to this property and user_id column in the database to the primary key of the user model.
    @JoinColumn(name="user_id", nullable = false)
    private User user;

    //    Constructors
//    Default constructors are required when retrieving data from the database.
    public Course(){}

    public Course(String name, String description, Double price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    //    Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public User getUser(){
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }
}
