package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
//This application will function as an endpoint that will be used in handling http request.
@RestController
//Will require all routes within the class to use the set endpoint as part of its route.
@RequestMapping("/greeting")
public class ActivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityApplication.class, args);
	}

//	ctrl + c to stop the server and application

//	Activity for s09 Seatwork
//	Create multiple routes with query strings and path variables.

//	1. Create a /welcome route that will accept multiple query string parameters (user & role) from a user and will display the following.
//	Multiple parameters
	// http://localhost:8080/greeting/welcome?user=value&role=value
	@GetMapping("/welcome")
	public String welcome(@RequestParam(value = "role") String role,
						  @RequestParam(value = "user") String user) {

		if (role.equals("admin") && user.equals("Denjikun")) {
			return "Welcome back to the class portal, Admin Denjikun!";
		}

		else if (role.equals("teacher") && user.equals("Angelito")) {
			return "Thank you for logging in, Teacher Angelito!";
		}

		else if (role.equals("student") && user.equals("Vladimir")) {
			return "Welcome to the class portal, Vladimir!";
		}

		else {
			return "Role out of range!";
		}
	}

//	2. Create a /register route that will accept multiply query string parameters (id, name, & course) from a user and will perform the following.
//  Multiple parameters
	// http://localhost:8080/greeting/register?id=value&name=value&course=value
	private List<Student> students = new ArrayList<>();

	@GetMapping("/register")
	public String register(@RequestParam int id,
						   @RequestParam String name,
						   @RequestParam String course) {

		Student student = new Student(id, name, course);
		students.add(student);
		return id + " your id number is registered on the system!";
	}

//	3. Create a /account/id route that will use Path Variable and will perform the following.
 	// http://localhost:8080/greeting/account/<id>
	@GetMapping("/account/{id}")
	public String getAccount(@PathVariable int id) {
		Optional<Student> student = students.stream()
				.filter(s -> s.getId() == id)
				.findFirst();

		if (student.isPresent()) {
			return "Welcome back " + student.get().getName() + "! You are currently enrolled in " + student.get().getCourse();
		} else {
			return "Your provided " + id + " is not found in the system!";
		}
	}

	private static class Student {
		private final int id;
		private final String name;
		private final String course;

		public Student(int id, String name, String course) {
			this.id = id;
			this.name = name;
			this.course = course;
		}

		public int getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public String getCourse() {
			return course;
		}
	}
}