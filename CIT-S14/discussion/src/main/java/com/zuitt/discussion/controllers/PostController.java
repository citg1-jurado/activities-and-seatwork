//Classes where services are mapped to their corresponding endpoints and HTTP methods via @RequestMapping annotation.

package com.zuitt.discussion.controllers;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.UserRepository;
import com.zuitt.discussion.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
//Enables cross origin request via @CrossOrigin.
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @Autowired
    post

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

//    Create Post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(String stringToken, @RequestBody Post post){
//        Retrieve the "User" object using the extracted username from the JWT Token
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        postService.createPost(post);

        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save;
        return new ResponseEntity<>("Post created successfully.", HttpStatus.CREATED);
    }

//    Get all posts
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPost(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

//    Delete a post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid){
        return postService.deletePost(postid);
    }

//    Update a post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatedPost(@PathVariable Long postid, @RequestBody Post post){
        return postService.updatePost(postid, post);
    }
}
